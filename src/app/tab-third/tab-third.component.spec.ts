/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { TabThirdComponent } from './tab-third.component';

describe('TabThirdComponent', () => {
  let component: TabThirdComponent;
  let fixture: ComponentFixture<TabThirdComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabThirdComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabThirdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
