import { Component, OnInit, Input } from '@angular/core';
import { FormService } from '../form.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

@Input() list:any[];

  constructor(private formService: FormService) {
  }

  ngOnInit() {
  }

}
