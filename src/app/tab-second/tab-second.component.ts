import { Component, OnInit, EventEmitter } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators
} from '@angular/forms';
import { FormService } from '../form.service';

@Component({
  selector: 'app-tab-second',
  templateUrl: './tab-second.component.html',
  styleUrls: ['./tab-second.component.scss']
})
export class TabSecondComponent implements OnInit {
  secondForm: FormGroup;
  name: FormControl;

  constructor(builder: FormBuilder, private formService:FormService) {
    this.name = new FormControl('name', [Validators.required, Validators.minLength(5)]);
    this.secondForm = builder.group({
      name: this.name
    });
  }

  ngOnInit() {
  }

  tabSecond() {
  this.formService.information.name = this.secondForm.value.name;

  }

}
